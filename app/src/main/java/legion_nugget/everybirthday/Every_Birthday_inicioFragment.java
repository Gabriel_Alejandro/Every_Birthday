package legion_nugget.everybirthday;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

/**
 * A placeholder fragment containing a simple view.
 */
public class Every_Birthday_inicioFragment extends Fragment {

    private TextView mtextdetails;
    private CallbackManager mcallbackmanager;
    private AccessTokenTracker mtokentracker;
    private ProfileTracker mprofileTracker;
    private Intent inic_notif;

    private boolean logeado = false;
    private Intent login_Y_N; //intent para verificar si el usuario se ha logueado o no.

    private FacebookCallback<LoginResult> mcallback = new  FacebookCallback<LoginResult>(){
        @Override
        public void onSuccess(LoginResult loginResult) {

            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            displaywelcomemessage(profile);
    /*
            logeado = true;

            login_Y_N = new Intent(Every_Birthday_inicioFragment.this.getActivity(), Every_Birthday_inicio.class);
            login_Y_N.putExtra("estado_logeo", logeado);
            startActivity(login_Y_N);*/
        }

        @Override
        public void onCancel() {
            // App code
        }

        @Override
        public void onError(FacebookException exception) {
            // App code
        }
    };




    public Every_Birthday_inicioFragment() {
    }

    @Override
    public  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        mcallbackmanager = CallbackManager.Factory.create();
        mtokentracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            }
        };

        mprofileTracker = new ProfileTracker(){
            @Override
            protected  void onCurrentProfileChanged(Profile oldprofile,Profile newprofile){
                displaywelcomemessage(newprofile);
            }
        };

        mtokentracker.startTracking();
        mprofileTracker.startTracking();
        /*
        login_Y_N = new Intent(Every_Birthday_inicioFragment.this.getActivity(), Every_Birthday_inicio.class);

        login_Y_N.putExtra("estado_logeo", logeado);
        startActivity(login_Y_N);*/


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_every__birthday_inicio, container, false);
    }

    private void displaywelcomemessage(Profile profile){

        if(profile != null){
            mtextdetails.setText("Bienvenido " + profile.getName()+ "Birthday:");
            String usernameF = profile.getName();
            /*Intent send_user_evento = new Intent(Every_Birthday_inicioFragment.this.getActivity(), Every_Birthday_Notificaciones.class);
            send_user_evento.putExtra("nombre_de_usuario", usernameF);
            startActivity(send_user_evento);*/
        }
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);
        mtextdetails = (TextView)view.findViewById(R.id.mtextdetails);
        LoginButton loginboton = (LoginButton) view.findViewById(R.id.login_button);
        //loginboton.setReadPermissions("user_friends","user_birthday");
        loginboton.setReadPermissions(Arrays.asList("user_friends", "user_birthday"));
        loginboton.setFragment(this);
        loginboton.registerCallback(mcallbackmanager,mcallback);
    }

    @Override
    public void onResume(){
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displaywelcomemessage(profile);
    }

    @Override
    public  void onStop(){
        super.onStop();
        mtokentracker.stopTracking();
        mprofileTracker.startTracking();

    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent Data ){
        super.onActivityResult(requestCode,resultCode,Data);
        mcallbackmanager.onActivityResult(requestCode,resultCode,Data);
    }

}
