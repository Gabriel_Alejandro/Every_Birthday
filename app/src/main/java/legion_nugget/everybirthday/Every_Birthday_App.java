package legion_nugget.everybirthday;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Usuario on 22/10/2017.
 */

public class Every_Birthday_App extends Application {

    @Override
    public void onCreate(){

        super.onCreate();

        printHashKey();

    }

    public void printHashKey()
    {

        try{
            PackageInfo info = getPackageManager().getPackageInfo("legion_nugget.everybirthday", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures){

                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());

                Log.d("LEGION_NUGGET", Base64.encodeToString(md.digest() , Base64.DEFAULT));
            }



        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
