package legion_nugget.everybirthday;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.Toast;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import legion_nugget.everybirthday.Objetos.FirebaseReferences;
import legion_nugget.everybirthday.Objetos.Objeto_evento;

public class Every_Birthday_Evento extends AppCompatActivity {

    private FirebaseDatabase BD_F;
    private DatabaseReference Database_Ref;
    private EditText et2, et3;
    private String ID = "1";

    //Variables edit text 24/10/2017
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_every__birthday__evento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button backnotif = (Button)findViewById(R.id.back_notif);
        backnotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentbn = new Intent (v.getContext(), Every_Birthday_inicio.class);
                startActivityForResult(intentbn, 1);
            }
        });

        // proviene del layout, son los campos de texto 24/10/2017
        et2 = (EditText) findViewById(R.id.editText2);
        et3 = (EditText) findViewById(R.id.editText3);

        AdminSQLiteOpenHelper base = new AdminSQLiteOpenHelper(this);
        SQLiteDatabase bd = base.getWritableDatabase();

        BD_F = FirebaseDatabase.getInstance();
        Database_Ref = BD_F.getReference(FirebaseReferences.database_name).push();
    }

    //FUNCIONES DE LA BASE DE DATOS MYSQLITE

    public void alta(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);
        SQLiteDatabase bd = admin.getWritableDatabase();

        //String usuario =(String)getIntent().getExtras().getSerializable("nombre_de_usuario");
        String usuario = "Gabo";
        String titulo = et2.getText().toString();
        String descripcion = et3.getText().toString();
        ContentValues registro = new ContentValues();
        Objeto_evento Registrator= new Objeto_evento(descripcion,titulo,usuario);

        registro.put("ID", ID);
        registro.put("titulo", titulo);
        registro.put("descripcion", descripcion);

        // los inserto en la base de datos
        bd.insert("Evento_Festivo", null, registro);
        Database_Ref.setValue(Registrator);
        // mDatabase.child("").child(userId).setValue(user);
        bd.close();  //cierro base de datos

        // ponemos los campos a vacío para insertar el siguiente Evento siempre y cuando no se tenga uno anteriormente creado
        et2.setText(""); et3.setText("");

        Toast.makeText(this, "Evento registrado exitosamente", Toast.LENGTH_SHORT).show();

    }

    // Hacemos búsqueda automatica del evento al cual ser unico tendra clave dni 0
    public void consulta(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);
        SQLiteDatabase bd = admin.getReadableDatabase();


        Cursor fila = bd.rawQuery("select titulo,descripcion from Evento_Festivo where ID=" + ID, null);

        if (fila!=null) { fila.moveToFirst();  et2.setText(fila.getString(0)); et3.setText(fila.getString(1));

        } else Toast.makeText(this, "No hay ningun evento registrado",Toast.LENGTH_SHORT).show();

        assert fila != null;
        fila.close();

        bd.close();
        /*
        if (fila!=null)  { fila.moveToFirst(); et2.setText(fila.getString(0)); et3.setText(fila.getString(1)); }
         else  { Toast.makeText(this, "No hay ningun evento registrado", Toast.LENGTH_SHORT).show(); }

        bd.close();*/
    }


    /* Método para dar de baja al Evento insertado*/
    public void baja(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);
        SQLiteDatabase bd = admin.getWritableDatabase();

        // aquí borro la base de datos del Evento por el dni
        int cant = bd.delete("Evento_Festivo", "ID=" + ID, null);
        bd.close();
        et2.setText(""); et3.setText("");

        if (cant == 1){Toast.makeText(this, "Evento eliminado", Toast.LENGTH_SHORT).show();}
           else{Toast.makeText(this, "No hay ningun evento registrado", Toast.LENGTH_SHORT).show();}
    }


    // Método para modificar la información del evento
    public void modificacion(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String nombre = et2.getText().toString();
        String ciudad = et3.getText().toString();

        ContentValues registro = new ContentValues();

        // actualizamos con los nuevos datos, la información cambiada
        registro.put("titulo", nombre);
        registro.put("descripcion", ciudad);

        int cant = bd.update("Evento_Festivo", registro, "ID=" + ID, null);
        bd.close();

        if (cant == 1)  { Toast.makeText(this, "Evento modificado con éxito", Toast.LENGTH_SHORT).show(); }
           else   { Toast.makeText(this, "No existe Evento",Toast.LENGTH_SHORT).show(); }
        et2.setText(""); et3.setText("");
    }


}
