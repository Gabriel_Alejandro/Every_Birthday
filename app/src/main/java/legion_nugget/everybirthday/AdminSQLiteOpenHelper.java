package legion_nugget.everybirthday;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Usuario on 24/10/2017.
 */

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper  {

    private static final String database_name = "Agenda_de_eventos.sqlite";
    private static final int Db_version = 1;

    public AdminSQLiteOpenHelper(Context context) {

        super(context, database_name, null, Db_version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //aquí creamos la tabla del evento
        db.execSQL("create table Evento_Festivo(ID integer primary key, titulo text, descripcion text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {

        db.execSQL("drop table if exists Evento_Festivo");
        db.execSQL("create table Evento_Festivo(ID integer primary key, titulo text, descripcion text)");

    }
}
