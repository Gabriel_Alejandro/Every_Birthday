package legion_nugget.everybirthday.Objetos;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Usuario on 19/11/2017.
 */

public class Objeto_evento {

    public String descripcion;
    public String titulo;
    public String usuario;

    public int starCount = 0;
    public Map<String, Boolean> stars = new HashMap<>();

    public Objeto_evento() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Objeto_evento(String descripcion, String titulo, String usuario) {
        this.descripcion = descripcion;
        this.titulo = titulo;
        this.usuario = usuario;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("Descripcion", descripcion);
        result.put("Titulo", titulo);
        result.put("Usuario",usuario);

        return result;
    }

}

