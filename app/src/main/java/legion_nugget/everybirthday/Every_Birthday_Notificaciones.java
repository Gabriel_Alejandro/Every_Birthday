package legion_nugget.everybirthday;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Every_Birthday_Notificaciones extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_every__birthday__notificaciones);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button evento = (Button) findViewById(R.id.go_event);
        evento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ir_evento = new Intent (v.getContext(),Every_Birthday_Evento.class);
                startActivityForResult(ir_evento,0);
            }
        });


        Button backinic = (Button)findViewById(R.id.back_inic);
        backinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent (v.getContext(), Every_Birthday_inicio.class);
                startActivityForResult(intent2, 1);
            }
        });

        //Aqui utilizaremos la el mes de Actual como ejemplo para nuestra notificacion

        Calendar mCalendar = Calendar.getInstance();
        String mesActual = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        String mes_mi_cumpleaños;

        //mes_mi_cumpleaños = (String)getIntent().getExtras().getSerializable("mes_seleccionado");

        mes_mi_cumpleaños = "noviembre";
        if(mesActual.equals(mes_mi_cumpleaños)){

            Toast.makeText(this, "Hoy es el mes " + mesActual+", acerca tu cumple", Toast.LENGTH_LONG).show();

            showNotification();
        }

    }

    public void showNotification() {
  /*
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(Every_Birthday_Notificaciones.this)
                        .setSmallIcon(android.R.drawable.stat_sys_warning)
                        .setLargeIcon((((BitmapDrawable)getResources()
                        .getDrawable(R.drawable.com_facebook_button_icon)).getBitmap()))
                        .setContentTitle("Mensaje de Alerta")
                        .setContentText("Ejemplo de notificación.")
                        .setTicker("Alerta!");

        Intent notIntent = new Intent(this, Every_Birthday_Notificaciones.class);

        PendingIntent contIntent = PendingIntent.getActivity(this, 0, notIntent, 0);

        mBuilder.setContentIntent(contIntent);*/

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

      //   mNotificationManager.notify(10, mBuilder.build());

        mNotificationManager.cancelAll();

    }
}


